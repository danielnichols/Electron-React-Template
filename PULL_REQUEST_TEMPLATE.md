Hello there! Welcome. Please follow the steps below to tell us about your contribution.

1. Copy the correct template for your contribution
  * Are you fixing a bug? Copy [this template](.github/PULL_REQUEST_TEMPLATE/bugfix.md)
  * Are you improving performance? Copy [this template](.github/PULL_REQUEST_TEMPLATE/perf.md)
  * Are you updating documentation? Copy [this template](.github/PULL_REQUEST_TEMPLATE/docs.md)
  * Are you changing functionality? Copy [this template](.github/PULL_REQUEST_TEMPLATE/behavior.md)
2. Replace this text with the contents of the template
3. Fill in all sections of the template
4. Click "Create pull request"
